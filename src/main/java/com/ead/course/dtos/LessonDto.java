package com.ead.course.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LessonDto {

    private String description;

    @NotBlank
    private String title;
    @NotBlank
    private String videoUrl;

}
