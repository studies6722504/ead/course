package com.ead.course.services.impl;

import com.ead.course.services.UtilsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UtilsServiceImpl implements UtilsService {

    @Value("${ead.api.url.authuser}")
    String REQUEST_URI;

    public String createUrlGetAllUsersByCourse(UUID userId, Pageable pageable) {
        return REQUEST_URI + "/users?courseId=" + userId + "&page=" + pageable.getPageNumber() + "&size="
                + pageable.getPageSize() + "&sort=" + pageable.getSort().toString().replaceAll(": ", ",");
    }

    public String createUrlGetOneUserById(UUID userId) {
        return REQUEST_URI + "/users/" + userId;
    }

    public String createUrlSaveSubscriptionUserInCourse(UUID userId) {
        return REQUEST_URI + "/users/" + userId + "courses/subscription";
    }
}
